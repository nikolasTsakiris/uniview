import sqlite3
import os
from pathlib import Path

dir_path = Path().absolute()
DATABASE_NAME = f'{dir_path}/db.sqlite'


def get_db():
    conn = sqlite3.connect(DATABASE_NAME)
    return conn

